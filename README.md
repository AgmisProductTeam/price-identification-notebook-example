# README #

### This repository is meant to help set up AWS Marketplace Price Identification model

For this reason it has methods for:
- loading model to instance
- preparing payload for request
- sending request
- displaying identified price